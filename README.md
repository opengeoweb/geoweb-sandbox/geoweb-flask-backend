# gitlab-oauth-example

Python Flask example of building GitLab OAuth.

## To create an IDP in gitlab:

- In the top-right corner, select your avatar.
- Select Edit profile.
- In the left sidebar, select Applications.
- Enter a Name, Redirect URI and OAuth 2 scopes (openid and email) as defined in Authorized Applications.
- The Redirect URI is the URL where users are sent after they authorize with GitLab.
- Select Save application and copy the application id and sercret.

## Set the env with these settings:

- `export GITLAB_CLIENT_ID=<client id from gitlab>`
- `export GITLAB_CLIENT_SECRET=<client secret from gitlab>`

## Start the backend flask app (this repo):
`python3 main.py`


## Frontend opengeoweb

The frontend is from https://gitlab.com/opengeoweb/opengeoweb/ 

Edit opengeoweb/apps/knmi-geoweb/src/assets/config.json, to make it look like:

```
{
  "AUTH_LOGIN_URL": "https://gitlab.com/oauth/authorize?client_id={client_id}&response_type=code&scope=email+openid&redirect_uri={app_url}/code",
  "AUTH_LOGOUT_URL": "https://gitlab.com/logout?client_id={client_id}&logout_uri={app_url}/login",
  "AUTH_TOKEN_URL": "http://localhost:5000/code",
  "AUTH_CLIENT_ID": "<client id from gitlab>",
  "INFRA_BASE_URL": "-",
  "APP_URL": "http://localhost:4200",
  "SW_BASE_URL": "-",
  "SIGMET_BASE_URL": "-",
  "AIRMET_BASE_URL": "-",
  "TAF_BASE_URL": "-"
}
```

Start the frontend with:

`nx serve knmi-geoweb`


## Learn More

GitLab as an OAuth2 provider: https://docs.gitlab.com/ee/api/oauth2.html

## OAuth2 flow used in these examples:

![](OAuthFlow.JPG)