import os
import logging
import sys
from flask import Flask, request
from flask_cors import CORS, cross_origin
import base64
import json
import requests

root = logging.getLogger()
root.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)

app = Flask(__name__)

CLIENT_ID = os.environ['GITLAB_CLIENT_ID']
CLIENT_SECRET = os.environ['GITLAB_CLIENT_SECRET']
TOKEN_URL = 'https://gitlab.com/oauth/token?'

@app.route("/code", methods=["POST"]) 
@cross_origin()
def callback():
    body = (json.loads(request.data))
    code = body['code']
    
    # Exchange the code for an access token, send the code to gitlab
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    payload = {'grant_type': 'authorization_code', 'client_id': CLIENT_ID,
              'client_secret': CLIENT_SECRET, 'redirect_uri': body["redirect_uri"], "code": body['code']}

    x = requests.post(TOKEN_URL, data = payload, headers=headers)
    accessTokenResponse = json.loads(x.text);
    
    # Get the id_token from the gitlab response
    idToken = accessTokenResponse['id_token']
    idTokenParts = idToken.split(".");
    decodedIdidToken = json.loads(base64.b64decode(idTokenParts[1] + '==='))
    
    print('decodedIdidToken', decodedIdidToken)
    
    # The geoweb frontend expect a username in the idtoken. We only have email, use this instead
    decodedIdidToken['username'] = decodedIdidToken['email']
    encodedIdToken = (base64.b64encode(json.dumps(decodedIdidToken).encode('ascii'))).decode('ascii')
    
    # The geoweb frontend expects an access_token, but in gitlab this one is called id_token. Put back the changed into access_token.
    accessTokenResponse['access_token'] = idTokenParts[0] + '.' + encodedIdToken + '.' + idTokenParts[2]

    # The geoweb expects the response to be wrapped inside a body.
    accessTokenBody = {"body":accessTokenResponse}

    response = app.response_class(
        response=json.dumps(accessTokenBody),
        status=200,
        mimetype='application/json'
    )
    return response


if __name__ == "__main__":
    # This allows us to use a plain HTTP callback
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = "1"

    app.secret_key = os.urandom(24)
    app.run(debug=True)